import React from 'react';

import Wrapper from '../../hoc/Wrapper';
import Footer from './Footer/Footer';

const Layout = ( props ) => (
	<Wrapper>
		{props.children}
		<Footer />
	</Wrapper>
);

export default Layout;