import React from 'react';
import {Container, Row, Col} from 'mdbreact';
import classes from './Header.module.css';

const Header = ( props ) => (
	<div className={classes.Header}>
		<Container>	
			<Row>
				<Col md='6'>
					<a href="#home"><h1 className={classes.Logo}>SM</h1></a>
				</Col>
				<Col md='6'>
					<nav className={classes.Navigation}>
						<a href="#home">Home</a>
						<a href="#about">About</a>
						<a href="#skills">Skills</a>
						<a href="#projects">Projects</a>
						<a href="#blog">My Blog</a>
						<a href="#contact">Contact</a>
					</nav>
				</Col>
			</Row>
		</Container>
	</div>
	);

export default Header;