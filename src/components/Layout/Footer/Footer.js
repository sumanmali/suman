import React from 'react';
import { Container, Row, Col, MDBIcon, MDBBadge } from "mdbreact";
import classes from './Footer.module.css';
import Button from '../../UI/Button/Button';


const Footer = ( props ) => {

	return (
		<section className={classes.Footer}>
			<Container>
				<Row>
					<Col md="3">
						<h5>Lets talk about</h5>
						<p>Learning and Teaching is quite difficult, and most difficult part comes when you need to do it in Real World.</p>
						<ul className={classes.SocialLinks}>
							<li>
								<a href="https://www.facebook.com/suman.mali.12" rel="noopener noreferrer author" target="_blank"><MDBIcon fab icon="facebook-f" /></a>
							</li>
							<li>
								<a href="https://www.instagram.com/v51.2.15/" rel="noopener noreferrer author" target="_blank"><MDBIcon fab icon="instagram" /></a>
							</li>
							<li>
								<a href="https://np.linkedin.com/in/suman-mali-531780100" rel="noopener noreferrer author" target="_blank"><MDBIcon fab icon="linkedin-in" /></a>
							</li>
							<li>
								<a href="skype:+9779840465291?call" rel="noopener noreferrer author" target="_blank"><MDBIcon fab icon="skype" /></a>
							</li>
						</ul>
					</Col>
					<Col md="3">
						<h5>Links</h5>
						<ul className={classes.ListGroup}>
						    <li className={ classes.List }>
						    	<a href="#home"><MDBIcon icon="long-arrow-alt-right" />Home</a></li>
						    <li className={ classes.List }>
						    	<a href="#about"><MDBIcon icon="long-arrow-alt-right" />About</a></li>
						    <li className={ classes.List }>
						    	<a href="#skills"><MDBIcon icon="long-arrow-alt-right" />Skills</a></li>
						    <li className={ classes.List }>
						    	<a href="#projects"><MDBIcon icon="long-arrow-alt-right" />Projects</a></li>
						    <li className={ classes.List }>
						    	<a href="#contact"><MDBIcon icon="long-arrow-alt-right" />Contact</a></li>
						  </ul>
					</Col>
					<Col md="3">
						<h5>Skills</h5>
						<ul className={classes.ListGroup}>
						    <li className={ classes.List }>
						    	<a href="#skills"><MDBIcon icon="long-arrow-alt-right" />Laravel
						    	<MDBBadge color="primary" pill>70%</MDBBadge></a>
						    </li>
						    <li className={ classes.List }>
						    	<a href="#skills"><MDBIcon icon="long-arrow-alt-right" />Vue
						    	<MDBBadge color="primary" pill>20%</MDBBadge></a>
						    </li>
						    <li className={ classes.List }>
						    	<a href="#skills"><MDBIcon icon="long-arrow-alt-right" />React
						    	<MDBBadge color="primary" pill>40%</MDBBadge></a>
						    </li>
						    <li className={ classes.List }>
						    	<a href="#skills"><MDBIcon icon="long-arrow-alt-right" />React Native
						    	<MDBBadge color="primary" pill>5%</MDBBadge></a>
						    </li>
						    <li className={ classes.List }>
						    	<a href="#skills"><MDBIcon icon="long-arrow-alt-right" />Wordpress
						    	<MDBBadge color="primary" pill>60%</MDBBadge></a>
						    </li>
						  </ul>
					</Col>
					<Col md="3">
						<h5>Have any Question</h5>
						<ul className={classes.ListGroup}>
							<li className={ classes.List }>
								<MDBIcon icon="map-marker-alt" />
								Ta:nani, Thecho - 12 <br/> &nbsp;&nbsp;&nbsp;&nbsp; Godawori Municipality <br/> &nbsp;&nbsp;&nbsp;&nbsp; Kathmandu, Nepal
							</li>
							<li className={classes.List}>
								<a href="tel:+9779840465291">
									<MDBIcon icon="mobile-alt" />+977 9840465291
								</a>
							</li>
							<li className={classes.List}>
								<a href="mailto:sumanmali1167@gmail.com">
									<MDBIcon icon="envelope" />sumanmali1167@gmail.com
								</a>
							</li>
						</ul>
					</Col>
				</Row>
				<div className={classes.Copyright}>
					<p>{new Date().getFullYear()} Copyright &copy; All Rights Reserved | Designed and Developed by <a href="/" > Suman Mali </a></p>
					<a href="#home">
						<Button type="opp">
							<MDBIcon icon="angle-up" />
						</Button>
					</a>
				</div>
			</Container>
		</section>
	);
}

export default Footer;