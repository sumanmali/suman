import React from 'react';
import {Container, Row, Col, MDBIcon, MDBInput } from 'mdbreact';

import classes from './Contact.module.css';
import Button from '../UI/Button/Button';

const Contact = (props) => (
	<section className={classes.Contact} id="contact">
		<Container>
			<h6>CONTACT</h6>
			<h1>Contact Me</h1>
			<p className={classes.DimPara}>Feel free to contact me with any suggestions and enquiries.</p>
			<Row className={classes.CTASection}>
				<Col md="6">
					<div className={classes.Img} ></div>
				</Col>
				<Col md="6" className={classes.NoPad}>
					<form className={classes.CTAForm}>
				        <div className="grey-text">
				          <MDBInput label="Your name" icon="user" group type="text" validate error="wrong" success="right" />
				          <MDBInput label="Your email" icon="envelope" group type="email" validate error="wrong" success="right" />
				          <MDBInput label="Subject" icon="tag" group type="text" validate error="wrong" success="right" />
				          <MDBInput type="textarea" rows="4" label="Your message" icon="pencil-alt" />
				        </div>
				        <div className="text-center">
					        <Button>
								Send
					            <MDBIcon far icon="paper-plane" className="ml-1" />
					        </Button>
				        </div>
				    </form>
				</Col>
			</Row>
		</Container>
	</section>
);

export default Contact;