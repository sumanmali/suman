import React from 'react';
import {Container, Row } from 'mdbreact';
import MeshPortfolio from '../Helpers/Portfolio/Portfolio';
import classes from './Portfolio.module.css';

const Portfolio = (props) => {

	const dataImg = [
    {
      id: 1,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg',
      cols: 1,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. am!'
    },
    {
      id: 2,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(72).jpg',
      cols: 2,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. mnis quia laboriosam.'
    },
    {
      id: 3,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(71).jpg',
      cols: 1,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.  Aut.'
    },
    {
      id: 4,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(74).jpg',
      cols: 2,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.  natus. Veniam, ipsam.'
    },
    {
      id: 5,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(75).jpg',
      cols: 2,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. onsequuntur impedit tempora.'
    },

    {
      id: 6,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(78).jpg',
      cols: 1,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. i reiciendis repellat tenetur.'
    },
    {
      id: 7,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(77).jpg',
      cols: 2,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. ssumenda eum!'
    },
    {
      id: 8,
      img:
        'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(79).jpg',
      cols: 1,
      title: 'Portfolio Title',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. doloribus veniam.'
    }
  ];

	return (
		<section className={classes.Portfolio} id="projects">
			<Container>
				<h6>ACCOMPLISHMENTS</h6>
				<h1>My Portfolio</h1>
				<p className={classes.DimPara}>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
				<Row>
					{
						dataImg.map(portfolio => {
							return <MeshPortfolio key={portfolio.id} data={portfolio} />
						})
					}
				</Row>
			</Container>
		</section>
		);
};

export default Portfolio;