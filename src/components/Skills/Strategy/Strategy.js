import React from 'react';
import {Container, Row } from 'mdbreact';
import Card from '../../UI/Card/Card';

import classes from './Strategy.module.css';

const Strategy = (props) => {

	const steps = [
		{
			id: 1,
			title: 'Explore',
			icon: 'searchengin',
			list1: 'Design Spirits',
			list2: 'Product Strategy',
			list3: 'UX Strategy',
		},
		{
			id: 2,
			title: 'Create',
			icon: 'code',
			list1: 'Information',
			list2: 'UX/UI Design',
			list3: 'Branding',
		},
		{
			id: 3,
			title: 'Learn',
			icon: 'medapps',
			list1: 'Prototyping',
			list2: 'User Testing',
			list3: 'UI Testing',
		},
	];

	return (
		<section className={classes.Strategy}>
			<Container>
				<h6>WHAT I DO</h6>
				<h1>Strategy, design and a bit of magic</h1>
				<p className={classes.DimPara}>First, document the requirements and then design it with full of creativity till you get satisfied!</p>
				<Row>
					{
						steps.map(step => {
							return <Card key={step.id} step={step}/>
						})
					}
				</Row>
			</Container>
		</section>
	);
};

export default Strategy;