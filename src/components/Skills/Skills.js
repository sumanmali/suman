import React from 'react';
import {Container, Row } from 'mdbreact';

import classes from './Skills.module.css';
import Strategy from './Strategy/Strategy';
import Wrapper from '../../hoc/Wrapper';
import ProgressBar from '../UI/ProgressBar/ProgressBar';

const Skills = (props) => {

	const mySkills = [
		{
			id: 1,
			title: 'HTML5',
			perfection: 80
		},
		{
			id: 2,
			title: 'CSS3',
			perfection: 75
		},
		{
			id: 3,
			title: 'jQuery',
			perfection: 60
		},
		{
			id: 4,
			title: 'Javascript',
			perfection: 70
		},
		{
			id: 5,
			title: 'Laravel',
			perfection: 70
		},
		{
			id: 6,
			title: 'Vue Js',
			perfection: 30
		},
		{
			id: 7,
			title: 'React Js',
			perfection: 60
		},
		{
			id: 8,
			title: 'React Native',
			perfection: 10
		}
	];

	return (
		<Wrapper>
			<section className={classes.Skills} id="skills">
				<Container>
					<h6>SKILLS</h6>
					<h1>My Skills</h1>
					<p className={classes.DimPara}>'Technical skill is mastery of complexity, while creativity is mastery of simplicity'. Use the skills wisely
					for one great and beautifully crafted art of web.</p>
					<Row>
						{
							mySkills.map(skill => {
								return <ProgressBar key={skill.id} skill={skill} />
							})
						}
					</Row>
				</Container>
			</section>
			<Strategy />
		</Wrapper>
	);
};

export default Skills;