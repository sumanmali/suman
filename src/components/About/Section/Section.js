import React from 'react';
import {Col, MDBIcon} from 'mdbreact';

import classes from './Section.module.css';

const Section = (props) => (
	<Col md="4" className={classes.Section}>
		<MDBIcon icon={props.section.icon} />
		<h4>{props.section.title}</h4>
		<p>{props.section.description}</p>
	</Col>
);

export default Section;