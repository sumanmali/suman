import React from 'react';
import {Container, Row, Col } from 'mdbreact';

import classes from './About.module.css';
import Section from './Section/Section';
import Image from '../Helpers/Image/Image';

const About = (props) => {

	const section = [
		{
			id: 1,
			icon: 'laptop-code',
			title: 'Website Design',
			description: 'User Friendly and fully Responsive'
		},
		{
			id: 2,
			icon: 'laptop-code',
			title: 'Web Applicaiton',
			description: 'Dynamic and User Friendly'
		},
		{
			id: 3,
			icon: 'mobile-alt',
			title: 'Mobile Application',
			description: 'Smooth and Inituitive User Interface'
		},
	];

	return (
		<section className={classes.About} id="about">
			<Container>
				<Row>
					<Col md="6">
						<div className={classes.ImageWrapper}>
							<Image img="about-1" alt="Suman Mali" />
						</div>
					</Col>
					<Col md="6" className={classes.Content}>
						<h6>WELCOME</h6>
						<h1>About Me</h1>
						<p className={classes.AboutContent}>I am a graduate from VTU with a degree of BE. in Computer Science and Engineering.
						 My field of expertise is Web development and designing. As JavaScript fantasizes me, I enjoy playing with JS so I am learning with Vue Js, React Js and React Native.</p>
						<p className={classes.AboutContent}>Besides my professional work, I would like to get myself lost in nature. For that, I play guitar and go a long tour.</p>
						<Row>
							{
								section.map(section => {
									return <Section key={section.id} section={section} />
								})
							}
						</Row>
					</Col>
				</Row>
			</Container>
		</section>
	);
};

export default About;