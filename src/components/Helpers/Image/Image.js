import React from 'react';

import image_1 from '../../../images/image_1.jpg';
import about_1 from '../../../images/about-1.jpg';
import classes from './Image.module.css';

const Image = (props) => {

	let img = null;

	switch(props.img){
		case 'image_1': 
			img = <img src={image_1} alt={props.alt} />
			break;
		case 'about-1': 
			img = <img src={about_1} alt={props.alt} />
			break;
		default:
			img =null;
	}

	return (
		<div className={classes.ImageHolder}>
			{img}
		</div>
		);
};

export default Image;