import React from 'react';

import classes from './Portfolio.module.css';

const Portfolio = (props) => (
	<div className={`${classes.Portfolio} col-md-${props.data.cols* 3} `} >
		<img src={props.data.img} alt={props.data.title} />
		<div className={classes.Overlay}>
			<h5>{props.data.title}</h5>
			<p>{props.data.description}</p>
		</div>
	</div>
);

export default Portfolio;