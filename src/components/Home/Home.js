import React from 'react';
import Header from '../Layout/Header/Header';
import HeaderFixed from '../Layout/HeaderFixed/HeaderFixed';
import {Container} from 'mdbreact';
import Button from '../UI/Button/Button';

import classes from './Home.module.css';

class Home extends React.Component{

	render(){

		let header = <Header />;
		if(this.props.headerType === 'fixed'){
			header = <HeaderFixed />;
		}

		return (
			<section className={classes.Home} id="home">
				{header}
				<Container>
					<div className={classes.Wrapper}>
						<h6><span>HELLO</span></h6>
						<div className={classes.Div}>
							<h5>I'm <b>Suman Mali</b> from <span>Nepal</span>.</h5><h5>A <span>Full Stack Developer</span>.</h5>
							<a href="#contact">
								<Button>
									HIRE ME
								</Button>
							</a>
						</div>
					</div>
				</Container>
			</section>
		);
	}
}

export default Home;