import React from 'react';
import { Col, MDBIcon } from 'mdbreact';

import Image from '../../Helpers/Image/Image';
import classes from './SinglePost.module.css';
import Button from '../../UI/Button/Button';

const SinglePost = (props) => {
	const months = [
		'January', 'February', 'March', 'April', 'May', 'June', 'July', 'Auguest', 'September', 'October', 'November', 'December'
	];

	const formatedDate = (d) => {
		const date = new Date(d);
		const month = date.getMonth();
		const day = date.getDate();
		const year = date.getFullYear();
		return months[month] + ' ' + day + ', ' + year;
	}

	return (
		<Col md="4">
			<div className={classes.SinglePost}>
				<Image img={props.postDetails.image} alt={props.postDetails.title} />
				<ul className={classes.ListDesc}>
					<li><MDBIcon far icon="calendar-alt" />{ formatedDate(Date()) }</li>
					<li><MDBIcon icon="user-alt" />{props.postDetails.author.toUpperCase()}</li>
					<li><MDBIcon icon="book-open" />{props.postDetails.time}</li>
				</ul>
				<h5>{props.postDetails.title}</h5>
				<p>{props.postDetails.description}</p>
				<div className="text-center">
					<Button>
						Read More
					</Button>
				</div>
			</div>
		</Col>
	);
};

export default SinglePost;