import React from 'react';
import {Container, Row } from 'mdbreact';

import SinglePost from './SinglePost/SinglePost';

import classes from './Blog.module.css';

const Blog = (props) => {
	
	const posts = [
		{
			id: 1,
			date: Date(),
			author: 'Suman',
			time: '3 min Read',
			title: 'Why Lead Generation is Key for Business Growth',
			image: 'image_1',
			description: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.'
		}, 
		{
			id: 2,
			date: Date(),
			author: 'Suman',
			time: '3 min Read',
			title: 'Why Lead Generation is Key for Business Growth',
			image: 'image_1',
			description: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.'
		}, 
		{
			id: 3,
			date: Date(),
			author: 'Suman',
			time: '3 min Read',
			title: 'Why Lead Generation is Key for Business Growth',
			image: 'image_1',
			description: 'A small river named Duden flows by their place and supplies it with the necessary regelialia.'
		}
	];

	return (
		<section className={classes.Blog} id="blog">
			<Container>
				<h6>BLOG</h6>
				<h1>My Blogs</h1>
				<p className={classes.DimPara}>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
				<Row className={classes.Posts}>
					{
						posts.map( post => {
							return <SinglePost key={post.id} postDetails={post}/>
						})
					}
				</Row>
			</Container>
		</section>
	);
};

export default Blog;