import React from 'react';

import classes from './Button.module.css';

const Button = (props) => {

	if(!props.type){
		return (
			<button className={classes.Button}>
				{props.children}
			</button>
		);
	}
	return (
		<button className={classes.ButtonO}>
			{props.children}
		</button>
	);
};

export default Button;