import React from 'react';
import { Col, MDBIcon } from 'mdbreact';
import classes from './Card.module.css';

const Card = (props) => (
	<Col md='4'>
		<div className={classes.Card}>
			<MDBIcon fab icon={props.step.icon} />
			<h5>{props.step.title}</h5>
			<ul>
				<li>{props.step.list1}</li>
				<li>{props.step.list2}</li>
				<li>{props.step.list3}</li>
			</ul>
		</div>
	</Col>
);

export default Card;