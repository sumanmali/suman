import React from 'react';
import {Col} from 'mdbreact';

import classes from './ProgressBar.module.css';

const ProgressBar = (props) => (
	<Col md='6'>
		<div className={classes.Label}>
			<label>{props.skill.title}</label>
			<p>{props.skill.perfection}%</p>
		</div>
		<div className={classes.ProgressBar}>
			<div className={classes.Progress} style={{width: props.skill.perfection + '%'}}></div>
		</div>
	</Col>
);

export default ProgressBar;