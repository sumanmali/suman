import React, {Component} from 'react';
import './App.css';

import Layout from './components/Layout/Layout';
import Home from './components/Home/Home';
import Contact from './components/Contact/Contact';
import Blog from './components/Blog/Blog';
import Portfolio from './components/Portfolio/Portfolio';
import Skills from './components/Skills/Skills';
import About from './components/About/About';
import Button from './components/UI/Button/Button';

class App extends Component {

  state = {
    home: <Home />
  }

  runOnScroll = () => {

    window.onscroll = () => {
      if(window.pageYOffset > 100){
          this.setState({
            home:  <Home headerType="fixed" />
          });
      }
      if(window.pageYOffset <= 100){
          this.setState({
            home:  <Home />
          });
        }
    }
   }

  render(){

     return (
      <div className="App" onLoad={ this.runOnScroll }>
        <Layout>
          {this.state.home}
          <About />
          <Skills />
          <div className="HireMe">
            <h3>I am <b>Available</b> for Freelancing.</h3>
            <a href="#contact">
              <Button type="opp">
                Hire Me
              </Button>
            </a>
          </div>
          <Portfolio />
          <Blog />
          <Contact />
        </Layout>
      </div>
    );
  }
 
}

export default App;